package com.example.examenc1;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import org.w3c.dom.Text;

public class CuentaBancoActivity extends AppCompatActivity {

    private EditText txtNumCuenta,txtNombre,txtBanco,txtSaldo, txtCantidad;
    private Spinner spiMovimientos;
    private TextView lblNuevoSaldo;
    private Button btnAplicar,btnRegresar, btnLimpiar;
    private CuentaBaco cb;
    private int pos = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cuenta_banco);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        initComponents();
        btnAplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtNumCuenta.isEnabled()){
                    if (txtNumCuenta.getText().toString().matches("")||txtNombre.getText().toString().matches("")||txtBanco.getText().toString().matches("")||txtSaldo.getText().toString().matches("")){
                        Toast.makeText(CuentaBancoActivity.this, "Faltaron datos por ingresar", Toast.LENGTH_SHORT).show();
                    } else {
                        cb.setNumCuenta(Integer.parseInt(txtNumCuenta.getText().toString()));
                        cb.setBanco(txtBanco.getText().toString());
                        cb.setNombre(txtNombre.getText().toString());
                        cb.setSaldo(Float.parseFloat(txtSaldo.getText().toString()));
                        txtSaldo.setEnabled(false);
                        txtNombre.setEnabled(false);
                        txtNumCuenta.setEnabled(false);
                        txtBanco.setEnabled(false);
                        spiMovimientos.setEnabled(true);
                        txtCantidad.setEnabled(true);
                    }
                } else {
                    switch (pos){
                        case 0: {
                            lblNuevoSaldo.setText("Saldo Actual: $"+cb.obtenerSaldo());
                            break;
                        }
                        case 1:{
                            if (txtCantidad.getText().toString().matches("")) {
                                Toast.makeText(CuentaBancoActivity.this, "Faltaron datos por ingresar", Toast.LENGTH_SHORT).show();
                            } else{
                                float nuevoSaldo = cb.retirarDinero(Float.parseFloat(txtCantidad.getText().toString()));
                                if (nuevoSaldo == -1) {
                                    Toast.makeText(CuentaBancoActivity.this, "No puedes retirar esta cantidad", Toast.LENGTH_SHORT).show();
                                } else {
                                    cb.setSaldo(nuevoSaldo);
                                }
                                lblNuevoSaldo.setText("Saldo Actual: $"+cb.getSaldo());
                            }
                            break;
                        }
                        case 2:{
                            if (txtCantidad.getText().toString().matches("")){
                                Toast.makeText(CuentaBancoActivity.this, "Faltaron datos por ingresar", Toast.LENGTH_SHORT).show();
                            } else{
                                cb.setSaldo(cb.hacerDeposito(Float.parseFloat(txtCantidad.getText().toString())));
                                lblNuevoSaldo.setText("Saldo Actual: $"+cb.getSaldo());
                            }
                            break;
                        }
                    }
                }
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtNumCuenta.isEnabled()){
                    txtNumCuenta.setText("");
                    txtBanco.setText("");
                    txtNombre.setText("");
                    txtSaldo.setText("");
                } else {
                    txtCantidad.setText("");
                    lblNuevoSaldo.setText("Saldo Actual: $");
                }
            }
        });
        spiMovimientos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pos = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // No hacer nada
            }
        });
    }

    private void initComponents(){
        txtBanco = (EditText) findViewById(R.id.txtBanco);
        txtCantidad = (EditText) findViewById(R.id.txtCantidad);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtSaldo = (EditText) findViewById(R.id.txtSaldo);
        txtNumCuenta = (EditText) findViewById(R.id.txtNumCuenta);
        lblNuevoSaldo = (TextView) findViewById(R.id.lblNuevoSaldo);
        spiMovimientos = (Spinner) findViewById(R.id.spiMovimientos);
        btnAplicar = (Button) findViewById(R.id.btnAplicar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.movimietos));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spiMovimientos.setAdapter(adapter);
        cb = new CuentaBaco();
    }

}