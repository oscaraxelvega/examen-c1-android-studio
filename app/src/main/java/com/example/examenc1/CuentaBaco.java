package com.example.examenc1;

import java.util.Random;

public class CuentaBaco {
    private String nombre, banco;
    private int numCuenta;
    private float saldo;

    public CuentaBaco(int numCuenta, String nombre, String banco, float saldo) {
        this.numCuenta = numCuenta;
        this.nombre = nombre;
        this.banco = banco;
        this.saldo = saldo;
    }

    public CuentaBaco() {
        this.numCuenta = 0;
        this.nombre = "";
        this.banco = "";
        this.saldo = 0.0f;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public int getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(int numCuenta) {
        this.numCuenta = numCuenta;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    public float obtenerSaldo(){
        return this.saldo;
    }

    public float retirarDinero(float cantidad){
        if(this.saldo < cantidad){
            return -1;
        } else {
            return saldo - cantidad;
        }
    }

    public float hacerDeposito(float cantidad){
        return this.saldo + cantidad;
    }

}
